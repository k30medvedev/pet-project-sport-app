SHELL := /bin/bash


NAME  := $(shell python .tools/pom.py artifactId)
TAG   ?= $(shell python .tools/pom.py version)
IMAGE := ${REPO}/${NAME}:${TAG}
GROUPID := $(subst .,/,$(shell .tools/pom.py groupId))

JAVA_SOURCE_FILES := $(shell find ./src -name '*.java')

USER_ID ?= $(shell stat -c "%u:%g" .)

## Applications
DOCKER_COMPOSE ?= IMAGE=${IMAGE} USER_ID=${USER_ID} docker-compose ${DOCKER_COMPOSE_OPTS} -f ./docker/docker-compose.build.yml

DOCKER ?= docker
MVN ?= ${DOCKER_COMPOSE} run --rm mvn -B

# Helpers
all: depend build


target/${NAME}-${TAG}.jar: pom.xml ${JAVA_SOURCE_FILES}
	${MVN} package org.apache.maven.plugins:maven-jar-plugin:test-jar source:jar-no-fork source:test-jar-no-fork -Dskip.unit.tests=true


build: target/${NAME}-${TAG}.jar


# QA
qa: validate checkstyle pmd

validate:
	${MVN} validate

checkstyle:
	${MVN} checkstyle:check

pmd:
	${MVN} pmd:check

.PHONY: qa validate checkstyle
