package by.kirill.sportsman.app.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import by.kirill.sportsman.app.config.swagger.run.*;
import by.kirill.sportsman.app.model.run.RunRequest;
import by.kirill.sportsman.app.model.run.RunResponse;
import by.kirill.sportsman.app.service.run.RunService;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/runs")
class RunController {

    private final RunService runService;

    @GetMapping
    @GetRunsOperation
    public ResponseEntity<List<RunResponse>> getAllRuns() {
        return ResponseEntity.ok(runService.getAllRuns());
    }

    @GetMapping("/{id}")
    @GetRunOperation
    public ResponseEntity<RunResponse> getRun(@PathVariable @Valid @Min(1) @Max(Integer.MAX_VALUE) final Long id) {
        return ResponseEntity.ok(runService.getRun(id));
    }

    @PostMapping
    @CreateRunOperation
    public ResponseEntity<RunResponse> createRun(final RunRequest request) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(runService.createRun(request));
    }

    @PutMapping("/{id}")
    @UpdateRunOperation
    public ResponseEntity<RunResponse> updateRun(@PathVariable @Valid @Min(1) @Max(Integer.MAX_VALUE) final Long id,
            final RunRequest request) {
        return ResponseEntity.accepted()
                .body(runService.updateRun(id, request));
    }

    @DeleteMapping("/{id}")
    @DeleteRunOperation
    public ResponseEntity<RunResponse> deleteRun(@PathVariable @Valid @Min(1) @Max(Integer.MAX_VALUE) final Long id) {
        return ResponseEntity.accepted()
                .body(runService.deleteById(id));
    }
}